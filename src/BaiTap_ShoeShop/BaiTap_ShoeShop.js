import React, { Component } from 'react'
import { shoeArr } from './data_shoeShop'
import ItemShoe from './ItemShoe'
import TableGioHang from './TableGioHang';

export default class BaiTap_ShoeShop extends Component {
    // b1: tạo state
    state = {
        shoeArr: shoeArr,
        // state shoeArr
        gioHang: [],
        // khai bao array gio hang, khi them 1 Item vao thi gio hang se hien 1 ptu trong arr
    }
    // render item ra giao dien, tach ham ra de goi ham lai vao return cuoi
    renderShoes = () => {
        return this.state.shoeArr.map((item) => {
            return <ItemShoe handleAddToCard={this.handleAddToCard} shoeData={item} key={item.id} />
        })
    };

    handleAddToCard = (sp) => {

        let index = this.state.gioHang.findIndex((item) => {
            return item.id === sp.id;
        });

        // không nên thay đổi trực tiếp dữ liệu của gioHang trong state , nên tạo 1 bản copy arr, gán cho thằng copy
        let cloneGioHang = [...this.state.gioHang];
        if (index === -1) {
            // th1: sp chưa có trong giỏ hàng => tạo object mới. có thêm key soLuong =1
            let newSp = { ...sp, soLuong: 1 }
            cloneGioHang.push(newSp);
        } else {
            // th2: sp đã có trong giỏ hàng, dựa vào index, tăng soLuong gioHang[index].soLuong++
            cloneGioHang[index].soLuong++;
        }
        this.setState({
            gioHang: cloneGioHang,
        })
    }

    handleRemoveShoe = (idShoe) => {
        let index = this.state.gioHang.findIndex((item) => {
            return item.id === idShoe;
        });

        if (index !== -1) {
            // xóa trên cloneGioHang
            let cloneGioHang = [...this.state.gioHang];
            cloneGioHang.splice(index, 1);
            this.setState({ gioHang: cloneGioHang })

        }
    };
    handleChangeQuantity = (idShoe, step) => {
        let index = this.state.gioHang.findIndex((item) => {
            return (item.id === idShoe);
        });
        let cloneGioHang = [...this.state.gioHang];
        cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + step;

        if (cloneGioHang[index].soLuong === 0) {
            cloneGioHang.splice(index, 1);
        }
        this.setState({ gioHang: cloneGioHang });
    }


    render() {
        return (
            <div className='container py-5'>
                {/* khi số lượng lớn hơn 0 mới render ra table, dùng && */}
                {this.state.gioHang.length > 0 && (
                    <TableGioHang
                        handleRemoveShoe={this.handleRemoveShoe}
                        gioHang={this.state.gioHang}
                        handleChangeQuantity={this.handleChangeQuantity} />)}

                {/* gọi ra renderShoes */}
                <div className='row'>{this.renderShoes()}</div>

            </div>
        )
    }
}
