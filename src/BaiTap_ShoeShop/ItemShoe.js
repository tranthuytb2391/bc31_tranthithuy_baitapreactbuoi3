// componnet chịu trách nhiệm render ra đôi giày

import React, { Component } from 'react'

// shoeData ~ props( đặt tên là shoeData)
export default class ItemShoe extends Component {
    render() {
        let { image, name, description } = this.props.shoeData;

        return (
            <div className="col-3">
                <div className="card" style={{ width: '100%' }}>
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        {/* toán tử ba ngôi */}
                        <p className="card-text">{description.length < 30
                            ? description
                            : description.slice(0, 30) + "..."}</p>
                        {/* nếu mô tả quá dài, thì dùng cách trên + "..." để ẩn mô tả. slice: cắt chuỗi */}
                        <button onClick={() => {
                            this.props.handleAddToCard(this.props.shoeData);
                        }}
                            className="btn btn-primary">Add to card</button>
                        {/* code react không dùng thẻ a */}
                    </div>
                </div>
            </div>
        )
    }
}
