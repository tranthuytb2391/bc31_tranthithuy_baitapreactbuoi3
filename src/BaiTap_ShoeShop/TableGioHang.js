import React, { Component } from 'react'

export default class TableGioHang extends Component {
    renderContent = () => {
        return this.props.gioHang.map((item) => {
            return <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.name}</td>
                <td>{item.price}</td>

                <td>
                    <button
                        className='btn btn-secondary'
                        onClick={() => this.props.handleChangeQuantity(item.id, -1)}
                    >-</button>
                    <span className='mx-2'>{item.soLuong}</span>
                    <button
                        className='btn btn-warning'
                        onClick={() => this.props.handleChangeQuantity(item.id, 1)}>+</button>
                </td>
                <td>
                    <button onClick={() => {
                        this.props.handleRemoveShoe(item.id);
                    }}
                        className='btn btn-danger'>Xoa</button>
                </td>
            </tr >
        })
    }
    render() {
        return (
            <div>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.renderContent()}
                    </tbody>
                </table>
            </div>
        )
    }
}
