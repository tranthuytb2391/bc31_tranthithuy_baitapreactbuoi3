import logo from './logo.svg';
import './App.css';
import BaiTap_ShoeShop from './BaiTap_ShoeShop/BaiTap_ShoeShop';

function App() {
  return (
    <div className="App">
      <BaiTap_ShoeShop />
    </div>
  );
}
export default App;
